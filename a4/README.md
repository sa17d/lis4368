# LIS 4368

## Sebastian Angel-Riano

### Assignment 4 Requirements:

*To Do List:*

1.  Server-Side Validation
2. Compile customer.java, CustomerServlet.java
3. Push
4. Post Deliverables on A4 README
5. Skillsets 10, 11, 12

*A4 README includes the following items:*


#### 1. Screenshot of Failed Validation 

![ServSideValiEMPTY](img_subdirectory/SSVE.png)

#### 2. Screenshot of Passed Validation 

![ServSideValiCOMPLETE](img_subdirectory/SSVC.png)

### Skillsets: Java

#### SS10. CountCharacters.java 

![CCJA](img_subdirectory/CCJA.png)

#### SS11. FileWriteReadCountWords.java

![FWRCWJA](img_subdirectory/FWRCWJA.png)

#### SS12. ascii.java 

![ASCIIJA1](img_subdirectory/asciiJA1.png)

![ASCIIJA2](img_subdirectory/asciiJA2.png)

![ASCIIJA3](img_subdirectory/asciiJA3.png)


