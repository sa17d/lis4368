# LIS 4368

## Sebastian Angel-Riano

### Project 2 Requirements:

*To Do List:*

1. Finish the fight.

*P2 README includes the following items:*


#### 1. Valid user form entry: customer form (customerform.jsp)

![P2E](img_subdirectory/p2entry.png)

#### 2. Screenshot of Passed Validation (thanks.jsp)

![P2EP](img_subdirectory/p2entrypass.png)

#### 3. Display Data

![P2Data](img_subdirectory/p2display.png)

#### 4. Modify/Update Form

![P2M](img_subdirectory/p2modify.png)

#### 5. Update Data Entry Sheet (Altered entry denoted in blue - see Notes section)

![P2U](img_subdirectory/p2updated.png)

#### 6. Delete Warning

![P2U](img_subdirectory/p2delete.png)

### Associated Database Changes in Terminal: Select, Insert, Update, Delete

#### Select (Before any changes)

![TMS](img_subdirectory/TMselect.png)

#### Insert: Added entry ID 12

![TMI](img_subdirectory/TMinsert.png)


#### Update: Again, see entry ID 12

![TMU](img_subdirectory/TMupdate.png)

#### Delete: Note entry ID 2 has been deleted from DB

![TMU](img_subdirectory/TMdelete.png)