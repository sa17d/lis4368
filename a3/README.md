# LIS 4368

## Sebastian Angel-Riano

### Assignment 3 Requirements:

*To Do List:*

1. Set up MySQL Workbench 4368 Repo 
2. A3 MWB File (pst, pet, cus table model)
3. Forward engineer!
4. Place .mwb file + SQL Script in A3>Docs
5. Answer Questions + SQL Statements

*A3 README includes the following items:*


#### 1. Screenshot of ERD 

![ERD](img_subdirectory/EERdiagramSS.png)

#### 2. Screenshot of A3/index.jsp

![A3index](img_subdirectory/a3indexSS.png)

#### 3. Links to A3.mwb, A3.sql

[A3 MWB File](docs/A3.mwb)

[A3 SQL File](docs/a3.sql)
