# LIS 4368

## Sebastian Angel-Riano

### Project 1 Requirements:

*To Do List:*

1. READMEs: MAIN + P1
2. Fix meta tags
3. Valdiation Stuff
4. Research Validation Codes
5. Push

*P1 README includes the following items:*


#### 1. Screenshot of Portal Landing page 

![PLD](img_subdirectory/p1splash.png)

#### 2. Screenshot of Validation Fail

![ValidationFAIL](img_subdirectory/p1validfail.png)

#### 3. Screenshot of Validation Pass

![ValidationPASS](img_subdirectory/p1validpass.png)