# LIS 4368

## Sebastian Angel-Riano

### Assignment 5 Requirements:

*To Do List:*

1. Include Server-Side Validation
2. Prepare READMEs
3. create data folder
4. create CustomerDB, ConnectionPool, and DBUtil
5. Compile, pray to God nothing explodes
6. Skillset
7. Fix CSS (probably later)

*A5 README includes the following items:*


#### 1. Valid user form entry: customer form (customerform.jsp)

![VLSForm](img_subdirectory/ValidFormSS.png)

#### 2. Screenshot of Passed Validation (thanks.jsp)

![ValidPASS](img_subdirectory/validPASS.png)

#### 3. Associated Database Entry

![DatabaseEntry](img_subdirectory/entryCONFIRM.png)

### Skillsets: Java

#### SS13: Number Swap

![SS13](img_subdirectory/NS.png)

#### SS14: Largest of 3 numbers

![SS14](img_subdirectory/L3N.png)


#### SS15: Simple Number Calc

![SS15](img_subdirectory/SNC.png)

