import java.io.*; // prove input/output streams used to read/write data to input/output sources
import java.util.Scanner;

public class FileWriteReadCountWords
{
    public static void main(String[] args) throws Exception
    {
        System.out.println("dev: Sebastian Angel-Riano");
        System.out.println("Program catches user input, writes to and reads from the same file, and counts number of words in file");
        System.out.println("Hint: use hasNext() method to read number of words (tokens).");
        System.out.println(); //print blank line

        String myFile = "filecountwords.txt";

        try {
            //create file object
            File file = new File(myFile); //takes file name as argument
            
            //create printwriter object
            PrintWriter writer = new PrintWriter(file); //takes file as an argument

            //create scnr obj to store user input
            Scanner input = new Scanner(System.in);

            //create string obj to store user input
            String str = "";

            System.out.print("Please enter text: ");
            str = input.nextLine();

            //write to file using PrintWriter write() method
            writer.write(str);

            System.out.println("Saved to file \"" + myFile + "\"");

            //close output file -- otherwise, open printWriter stream
            writer.close();

            //Scanner read = new Scanner(new FileInputStream(file));
            Scanner read = new Scanner (new FileInputStream(file));
            int count=0;
            while (read.hasNext())
            {
                read.next();
                count++;
            }

            System.out.println("Number of words: " + count);
            }

            catch(IOException ex)
            {
                System.out.println("Error writing to file " + myFile + "");
            }


        }
}
