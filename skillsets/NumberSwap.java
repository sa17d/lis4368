import java.util.InputMismatchException;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner6;
 
class NumberSwap
{
   public static void main(String args[])
   {
      int x=0, y=0, temp;
      System.out.println("Please enter first number:");
      Scanner in = new Scanner(System.in);
      
      
     boolean xIsInt = false;
     boolean yIsInt = false;
     
     while(xIsInt == false) 
     {
         try 
         {
             x = in.nextInt();
             xIsInt = true;

         }//end try 
         catch (InputMismatchException e)
         {
           System.out.println("Not valid integer!");
           System.out.println("Please try again. Enter first number:");
           in = new Scanner(System.in);
         }//end catch
    }//end while
     
     System.out.println("Please enter second number:");
     in = new Scanner(System.in);
     
     while(yIsInt == false) 
     {
         try 
         {
             y = in.nextInt();
             yIsInt = true;

         }//end try
         catch (InputMismatchException e)
         {
           System.out.println("Not valid integer!");
           System.out.println("Please try again. Enter second number:");
           in = new Scanner(System.in);
         }//end catch
    }//end while

     
      System.out.println("Before Swapping\nx = "+x+"\ny = "+y);
      
      temp = x;
      x = y;
      y = temp;
     
      System.out.println("After Swapping\nx = "+x+"\ny = "+y);
      

      
   }//end main
}//end NumberSwap
