import java.util.InputMismatchException;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner6;

class LargestThreeNumbers
{
  public static void main(String args[])
  {
    int x=0, y=0, z =0, temp;
      System.out.println("Please enter first number:");
      Scanner in = new Scanner(System.in);
      
      
     boolean xIsInt = false;
     boolean yIsInt = false;
     boolean zIsInt = false;
     
     while(xIsInt == false) 
     {
         try 
         {
             x = in.nextInt();
             xIsInt = true;

         }//end try 
         catch (InputMismatchException e)
         {
           System.out.println("Not valid integer!");
           System.out.println("Please try again. Enter first number:");
           in = new Scanner(System.in);
         }
    }
     
     System.out.println("Please enter second number:");
     in = new Scanner(System.in);
     
     while(yIsInt == false) 
     {
         try 
         {
             y = in.nextInt();
             yIsInt = true;

         }//end try
         catch (InputMismatchException e)
         {
           System.out.println("Not valid integer!");
           System.out.println("Please try again. Enter second number:");
           in = new Scanner(System.in);
         }
    }

    System.out.println("Please enter third number:");
    in = new Scanner(System.in);
    
    while(zIsInt == false) 
    {
        try 
        {
            z = in.nextInt();
            zIsInt = true;

        }//end try
        catch (InputMismatchException e)
        {
          System.out.println("Not valid integer!");
          System.out.println("Please try again. Enter third number:");
          in = new Scanner(System.in);
        }
   }
 
    if (x > y && x > z)
      System.out.println("First number is largest.");
    else if (y > x && y > z)
      System.out.println("Second number is largest.");
    else if (z > x && z > y)
      System.out.println("Third number is largest.");
    else
      System.out.println("Two or more numbers are not distinct.");
  }
}