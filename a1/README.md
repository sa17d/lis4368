# LIS 4368

## Sebastian Angel-Riano

### A1 # Requirements:

*Deliverables for A1:*


#### README.md file should include the following items:

*Git command list*

1. git init - create a new local repository
2. git status - checking files changed or ones needing add/commit
3. git add - add file(s) to staging
4. git commit - git equivalent of a "save"
5. git push - send changes to master branch of remote repository
6. git pull - fetch and merge chanegs on the remote server to your remote directory
7. git grep - Search command followed by desired entity or value in quotes "like this"

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img_subdirectory/amppsSS.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img_subdirectory/javahelloSS.png)

*Screenshot of local lis4368 http://localhost:9999/lis4368*:

![Screenshot](img_subdirectory/tomcaton9999SS.png)

*Screenshot of local lis4368/a1 http://localhost:9999/lis4368/a1/index.jsp*:

![Screenshot](img_subdirectory/runningA1SS.png)

[Link to Web App](http://localhost:9999/lis4368):

[Link to Bitbucketstation](https://bitbucket.org/sa17d/bitbucketstationlocations/src/master/):