<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Tue, 01-12-21, 16:10:09 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Content for Assignment1.">
	<meta name="author" content="Sebastian Angel-Riano">
	<link rel="icon" href="img_subdirectory/favicon.png">

	<title>LIS4368 - Assignment1</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body>
	<style>
		body {
		  background-color: black;
		  color: white;
		}
		</style>
<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<h4>JDK Installation:</h4>
					<img src="img_subdirectory/javahelloSS.png" class="img-responsive center-block" alt="JDK Installation" />

					<br /> <br />
					<b>Tomcat Installation:</b><br />
					<img src="img_subdirectory/tomcaton9999SS.png" class="img-responsive center-block" alt="Tomcat Installation" />

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		

</body>
</html>
