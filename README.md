# LIS 4368 Advanced Web App Development

## Sebastian Angel-Riano

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Git 
    - Configure Bitbucket
    - Complete Bitbucket Tutorial
    - Set up SSH
    - Install AMPPS
    - Install JDK
    - Run and screenshot Hello World 
    - Install Tomcat
    - Tomcat backups->config
    - cd to local tomcat subdir
    - Clone assignment starter files
    - Review subdirs
    - Review and edit index.jsp
    - Push local changes to remote
    - Provide read-only access to Bitbucket Repository
   
     [](Checkpoint...done.)



2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Actually make an A2 README
    - MySQL Install/Setup
    - Client login using AMPPS
    - Develop serverlet
    - Refer to debug log
    - Compile serverlet files
    - Answer Questions
     
     [](Checkpoint...done.)

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Set up MySQL Workbench 4368 Repo 
    - A3 MWB File (pst, pet, cus table model)
    - Forward engineer!
    - Place .mwb file + SQL Script in A3>Docs
    - Answer Questions + SQL Statements 
     
     [](Checkpoint...done.)
     
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - READMEs: MAIN (You are Here!) + P1
    - Fix meta tags
    - Valdiation Stuff
    - Research Validation Codes
    - Push

    [](Checkpoint...done.)

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server-Side Validation
    - Compile customer.java, CustomerServlet.java
    - Push
    - Skillsets
    - Post Deliverables on A4 README

    [](Checkpoint...done.)    

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server-side validation
    - Create data folder
    - create CustomerDB, ConnectionPool, and DBUtil
    - Compile, pray to God nothing explodes
    - Skillsets
   
    [](Checkpoint...done.)   

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Valid User Form Entry
    - Validation Pass
    - Display Data
    - Modify Form
    - Modified Data (Side note: highlight changes)
    - Delete Warning
    - Associated DB Changes
    
    [](Please see Post-Game Carnage Report for final debrief.)

8. Other things that need addressing, to be done in class debrief (May 2021)
    - Uniform CSS fine mechanics (Fonts, Colors, Navbar Colors)
    - Make last pass to make all pages current in UX 
    - Improve in-client experience