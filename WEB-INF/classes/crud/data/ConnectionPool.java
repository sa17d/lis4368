package crud.data;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ConnectionPool
{
    private static ConnectionPool pool = null;
    private static DataSource dataSource = null;

private ConnectionPool()
{
    try
        {
            InitialContext ic = new InitialContext();
            dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/sa17d");
        }
        catch (NamingException e)
            {
                System.out.println(e);
            }
}

    public static synchronized ConnectionPool getInstance()
    {
        if (pool == null)
            {
                pool = new ConnectionPool();
            }
        return pool;
    }

    public Connection getConnection()
    {
        try
            {
                return dataSource.getConnection();
            }
        catch (SQLException e)
        {
            System.out.println(e);
            return null;
        }
    }

    public void freeConnection(Connection c)
    {
        try
            {
                c.close();
            }
        catch (SQLException e)
            {
                System.out.println(e);
            }
    }
}

/* connection pooling:
opening/closing database connections is resource intensive. 
connection pooling improves DB performance, associated execution of commands, and facilitates reuse of connection objects, 
instead of openning/closing many connections to serve a number of client requests. 

JNDI - Java Naming and Directory Interface. Look it up. Tomcat uses JNDI

InitialContext is configured as a web application is initially deployed

begin with java:/comp/env, then add the remaining context value from the name attribute of the context.xml file in META-INF
name="jdbc/sa17d"
full InitialContext lookup() would be:

ic.lookup("java:/comp/env/jdbc/sa17d")
*/
