# LIS 4368

## Sebastian Angel-Riano

### Assignment 2 Requirements:

*To Do List:*

1. Actually make an A2 README
2. MySQL Install/Setup
3. Client login using AMPPS
4. Develop servlet
5. Refer to debug log
6. Compile serverlet files
7. Show results in A2 readme

#### README.md file should include the following items:

 *Links to the following:* 
 
 [http://localhost:9999/hello](http://localhost:9999/hello)

 [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)

 [http://localhost:9999/hello/querybook](http://localhost:9999/hello/querybook)

![QBSS](img_subdirectory/querybookSS.png)


#### Assignment Screenshot of Query Working:

![screenshot](img_subdirectory/querySS.png)

#### Screenshot of A2/index.jsp working

![screenshot](img_subdirectory/A2indexSS.png)